<?php 
  
   include 'conexao/conectaDB.php';

  if (!empty($_POST)) {
    // keep track validation errors
   

    // keep track post values
    $nome          = $_POST['nome'];
    $dataNasc      = $_POST['dataNasc'];
    $logradouro    = $_POST['logradouro'];
    $numero        = $_POST['numero'];
    $bairro        = $_POST['bairro'];
    $cidade        = $_POST['cidade'];
    $estado        = $_POST['estado'];
    $dataCria      = $_POST['dataCria'];
    $cep           = $_POST['cep'];
    
    
    // insert data
    $sql = "INSERT INTO aluno ( nome, dataNasc, logradouro , numero, bairro, cidade, estado, dataCria, cep) 
    VALUES ('$nome','$dataNasc', '$logradouro',' $numero ', '$bairro', '$cidade', '$estado', '$dataCria', '$cep')";
    $resultado = mysqli_query($conecta,$sql);

 
  }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<title>Cadastrar Aluno</title>
  <link rel="icon" href="" type="image/png" sizes="">
  <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> 
  <link rel="stylesheet" href="css/styles.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootbox.min.js"></script>
  <script src="js/bootbox.js"></script> 
</head>
<!-- date -->
<script src="js/bootstrap-datepicker.js"></script> 
<link rel="stylesheet" href="css/datepicker.css" media="screen"/>
<script type="text/javascript">
$(function(){
 var options = new Array();
 options['language'] = 'pt-BR';
 $('.datepicker').datepicker(options);
})
</script>
<style type="text/css">
    .help-block {
    line-height: 0px;
    color: #FF0000 ;
    display:inline-block;
    }
</style>
<body>
<nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="">CRUD</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                  <li><a href="index.php"> Sair</a></li>
            </ul>
              <ul class="nav navbar-nav">
                  <li><a href="criarAluno.php"> Criar Aluno</a></li>
            </ul>
             <ul class="nav navbar-nav">
                  <li><a href="criarProf.php"> Criar Professor</a></li>
            </ul>
             <ul class="nav navbar-nav">
                  <li><a href="criarCurso.php"> Criar Curso</a></li>
            </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="col-xs-6 col-centered">
      <div class="row">
        <h3>Criar um Aluno</h3>
      </div>
      <form class="form" action="criarAluno.php" method="post">

      <div class="col-xs-12">
        <div class="form-group ">
          <label class="control-label">Nome</label>
          <div class="form">
              <input name="nome" required class="form-control" type="text"  placeholder="Nome">
               
          </div>
        </div>
      </div>

      <div class="col-xs-6">
        <div class="form-group">
          <label class="control-label">Data Nascimento</label>
          <div class="form">
              <input name="dataNasc" type="date" required class="form-control" type="text">

          </div>
        </div>
      </div>

       <div class="col-xs-6">
        <div class="form-group ">
          <label class="control-label">CEP</label>
          <div class="form">
              <input name="cep" required class="form-control"  placeholder="cep">

          </div>
        </div>
      </div>
      
      <div class="col-xs-6">
        <div class="form-group ">
          <label class="control-label">Logradouro</label>
          <div class="form">
              <input name="logradouro" required class="form-control"  placeholder="logradouro">

          </div>
        </div>
      </div>

      <div class="col-xs-4">
        <div class="form-group ">
          <label class="control-label">Número</label>
          <div class="form">
              <input name="numero" required class="form-control" placeholder="numero">

          </div>
        </div>
      </div>

        <div class="col-xs-6">
        <div class="form-group ">
          <label class="control-label">Bairro</label>
          <div class="form">
              <input name="bairro" required class="form-control"  placeholder="bairro">

          </div>
        </div>
      </div>

      <div class="col-xs-6">
        <div class="form-group ">
          <label class="control-label">Cidade</label>
          <div class="form">
              <input name="cidade" required class="form-control"  placeholder="cidade">

          </div>
        </div>
      </div>

          <div class="col-xs-6">
        <div class="form-group ">
          <label class="control-label">Estado</label>
          <div class="form">
              <input name="estado" required class="form-control"  placeholder="estado">

          </div>
        </div>
      </div>

      <div class="col-xs-6">
        <div class="form-group">
          <label class="control-label">Data Criação</label>
          <div class="form">
              <input name="dataCria" type="date" required class="form-control" type="date">

          </div>
        </div>
      </div>

      

      <div class="col-xs-7">
        <div class="form-actions">
          <button type="submit" class="btn btn-success">Criar</button>
        </div>
      </div>
    </div> 
    </form>                                                       
    </div> 
</body>
</html>