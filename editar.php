  <?php
//  session_start();
  //if(isset($_SESSION['cpf'])) {
  //if(isset($_SESSION['nome'])) {

   include 'conexao/conectaDB.php';

   // $id_curso = filter_input(INPUT_GET, 'id_curso', FILTER_SANITIZE_NUMBER_INT);
  // $result_curso = "SELECT * FROM curso where id_curso = '$id_curso'";
  //  $resultado_curso = mysqli_query($conecta,$result_curso);
  // $row_curso = mysqli_fetch_assoc($resultado_curso);

   $id_curso = filter_input(INPUT_GET, "id_curso");
   $nomeCurso = filter_input(INPUT_GET, "nomeCurso");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<title>Editar</title>
  <link rel="icon" href="img/casa.png" type="image/png" sizes="">
  <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> 
  <link rel="stylesheet" href="css/styles.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootbox.min.js"></script>
  <script src="js/bootbox.js"></script> 
  
</head>
<!-- date -->
<script src="js/bootstrap-datepicker.js"></script> 
<link rel="stylesheet" href="css/datepicker.css" media="screen"/>
<script type="text/javascript">
$(function(){
 var options = new Array();
 options['language'] = 'pt-br';
 $('.datepicker').datepicker(options);
})
</script>
<style type="text/css">
    .help-block {
    line-height: 0px;
    color: #FF0000 ;
    display:inline-block;
    }
</style>
<body>
<nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="">CRUD</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                  <li><a href="index.php"> Sair</a></li>
            </ul>
              <ul class="nav navbar-nav">
                  <li><a href="criarAluno.php"> Criar Aluno</a></li>
            </ul>
             <ul class="nav navbar-nav">
                  <li><a href="criarProf.php"> Criar Professor</a></li>
            </ul>
             <ul class="nav navbar-nav">
                  <li><a href="criarCurso.php"> Criar Curso</a></li>
            </ul>
      </div>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="col-xs-6 col-centered">
      <div class="row">
        <h3>Criar um Usuário</h3>
      </div>
      <form class="form" action="proce_editar.php" method="post">

        
              <input name="id_curso"  type="hidden"  placeholder="" value="<?php echo $id_curso ?>">
               
          

      <div class="col-xs-12">
        <div class="form-group ">
          <label class="control-label">Nome Curso</label>
          <div class="form">
              <input name="nomeCurso" required class="form-control" type="text"  placeholder="" value="<?php echo $nomeCurso ?>">
               
          </div>
        </div>
      </div>
      
   
      <div class="col-xs-6">
        <div class="form-actions">
          <button type="submit" class="btn btn-success">Editar</button>
        </div>
      </div>
    </div> 
    </form>                                                       
    </div> 
</body>
</html>  

